(ns aoc.core
  (:require [aoc.day1 :as day1]
            [aoc.day2 :as day2])
  (:gen-class))

(defn -main
  "run the solution for the specified day"
  [& args]
  (if (nil? args)
    (println "None selected!")
    (case (first args)
      "1" (println "Solutions for day 1 are:" (day1/run))
      "2" (println "Solutions for day 2 are:" (day2/run))
      (println "Unknown selection:" (first args)))))
