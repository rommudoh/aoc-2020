(ns aoc.day2
  (:require [clojure.string :as str]))

(defn read-input
  ([] (read-input "resources/day2-input"))
  ([filename]
   (with-open [reader (clojure.java.io/reader filename)]
     (->> reader
        (line-seq)
        (reduce conj [])
        (map #(str/split % #"[- :] ?"))
        (map (fn [[a b & rest]] (conj rest
                                     (read-string b)
                                     (read-string a))))))))

(defn valid? [a b ch pw]
  (let [matches (re-seq (re-pattern ch) pw)
        cnt (count matches)]
    (and (>= cnt a) (<= cnt b))))

(defn other-valid? [a b ch pw]
  (let [p1 (= ch (str (get pw (dec a))))
        p2 (= ch (str (get pw (dec b))))]
    (or (and p1 (not p2)) (and (not p1) p2))))

(defn run
  ([] (list (run valid?) (run other-valid?)))
  ([f] (->> (read-input)
          (filter (partial apply f))
          (count))))
