(ns aoc.day1)

(defn read-input
  ([] (read-input "resources/day1-input"))
  ([filename]
   (with-open [reader (clojure.java.io/reader filename)]
     (map #(Integer. %)
          (reduce conj [] (line-seq reader))))))

(defn find-n-with-sum [n total coll]
  (if (= n 1)
    (let [element (some #{total} coll)]
      (if (nil? element)
        '()
        (cons (some #{total} coll) '())))
    (loop [element (first coll)
           coll-rest (rest coll)]
      (if (= '() coll-rest)
        coll-rest
        (let [result (find-n-with-sum (dec n) (- total element) coll-rest)]
          (if (= '() result)
            (recur (first coll-rest) (rest coll-rest))
            (cons element result)))))))

(defn run
  ([] (list (run 2) (run 3)))
  ([n] (->> (read-input)
          (find-n-with-sum n 2020)
          (apply *))))
