(ns aoc.core-test
  (:require [clojure.test :refer :all]
            [aoc.core :refer :all]))

(deftest test-none-selected
  (testing "Without Parameter it should print 'None selected!'"
    (with-redefs [println str]
      (is (= "None selected!" (aoc.core/-main))))))

(deftest test-invalid-argument
  (testing "Invalid argument"
    (let [arg "xxx"]
      (with-redefs [println str]
        (is (= (str "Unknown selection:" arg) (aoc.core/-main arg)))))))
