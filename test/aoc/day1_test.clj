(ns aoc.day1-test
  (:require [aoc.day1 :as day1]
            [clojure.test :refer :all]))

(deftest test-find-n-with-sum
  (testing "find 1 with sum"
    (is (= '(3) (day1/find-n-with-sum 1 3 [1 2 3 4 5]))))
  (testing "find 1 with sum, no match"
    (is (= '() (day1/find-n-with-sum 1 3 [1 2 4 5]))))
  (testing "find 2 with sum"
    (is (= '(2 3) (day1/find-n-with-sum 2 5 [1 2 3 5 6]))))
  (testing "find 2 with sum, no match"
    (is (= '() (day1/find-n-with-sum 2 9 [1 2 3 4]))))
  (testing "find 3 with sum"
    (is (= '(2 3 4) (day1/find-n-with-sum 3 9 [1 2 3 4])))))
