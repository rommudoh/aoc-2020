(ns aoc.day2-test
  (:require [aoc.day2 :as day2]
            [clojure.test :refer :all]))

(deftest test-valid-password
  (testing "valid password"
    (is (day2/valid? 1 3 "b" "abcde"))
    (is (day2/valid? 2 9 "c" "ccccccccc"))))

(deftest test-invalid-password
  (testing "invalid password"
    (is (not (day2/valid? 1 3 "b" "cdefg")))
    (is (not (day2/valid? 2 3 "b" "abcde")))))

(deftest test-other-valid-password
  (testing "valid password v2"
    (is (day2/other-valid? 1 3 "a" "abcde"))))

(deftest test-other-invalid-password
  (testing "invalid password v2"
    (is (not (day2/other-valid? 1 3 "b" "cdefg")))
    (is (not (day2/other-valid? 2 9 "c" "ccccccccc")))))
